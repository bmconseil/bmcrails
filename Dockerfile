# docker buildx build --platform linux/amd64,linux/arm64 -t registry.gitlab.com/bmconseil/bmcrails-multiarch:0.24.0 --push .

# Docker headers
# FROM ruby:2.6.5-slim-buster
FROM debian:buster-slim
# ARG RUBY_VERSION=2.7.1-jemalloc
ARG RUBY_VERSION=2.6.5-jemalloc
LABEL maintainer="Bruno MEDICI <opensource@bmconseil.com>"


# Environment
ENV LANG=C.UTF-8 \
    LANG=C.UTF-8 \
    RAILS_ENV=production \ 
    RACK_ENV=production \
    RAILS_LOG_TO_STDOUT=true \
    INSTALL_PATH=/app
WORKDIR $INSTALL_PATH


# Packages
RUN apt-get update && apt-get install -y --no-install-recommends \
  curl gnupg apt-transport-https ca-certificates \
  nodejs gcc g++ git build-essential \
  libvips42 tzdata zlib1g-dev liblzma-dev  \
  && apt-get autoremove --assume-yes
  # Bundler, bundle config

RUN apt-get update && apt-get install -y --no-install-recommends \
  ruby-dev 



# Fullstaq Ruby
RUN curl -SLf https://raw.githubusercontent.com/fullstaq-labs/fullstaq-ruby-server-edition/master/fullstaq-ruby.asc | apt-key add - \
    && echo "deb https://apt.fullstaqruby.org debian-10 main" > /etc/apt/sources.list.d/fullstaq-ruby.list
RUN apt-get update -q \
    && apt-get install -y -q --no-install-recommends fullstaq-ruby-${RUBY_VERSION} \
    && apt-get autoremove -y \
    && rm -fr /var/cache/apt
ENV GEM_HOME /usr/local/bundle
ENV BUNDLE_PATH="$GEM_HOME" \
    BUNDLE_SILENCE_ROOT_WARNING=1 \
    BUNDLE_APP_CONFIG="$GEM_HOME" \
    RUBY_VERSION=$RUBY_VERSION \
    LANG=C.UTF-8 LC_ALL=C.UTF-8
# path recommendation: https://github.com/bundler/bundler/pull/6469#issuecomment-383235438
ENV PATH $GEM_HOME/bin:$BUNDLE_PATH/gems/bin:/usr/lib/fullstaq-ruby/versions/${RUBY_VERSION}/bin:$PATH
CMD [ "irb" ]

RUN gem install bundler:2.1.4 \
  && bundle config --global frozen 1 \
  && bundle config git.allow_insecure true \
  && bundle config --use-system-libraries \
  # Cleanup
  && rm -rf /var/cache/apt /var/lib/apt/lists/* \
  # Prepare dir
  && mkdir -p $INSTALL_PATH
  


# Bundler and basic gems
ADD Gemfile* *.gemspec $INSTALL_PATH/
RUN bundle install --without="development test" -j4 --retry 5 --path=vendor \
  && rm -rf /usr/local/bundle/cache/*.gem \
  && find /usr/local/bundle/gems/ -name "*.c" -delete \
  && find /usr/local/bundle/gems/ -name "*.o" -delete


# Basic health checks
HEALTHCHECK --interval=30s --timeout=10s --start-period=10s --retries=2 \
  CMD curl --fail http://localhost/check || exit 1

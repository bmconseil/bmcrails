class Bmcrails::RegistrationsController < Devise::RegistrationsController

  def create
    puts "RegistrationsController#create"

    begin
      # Process this registration
      build_resource(sign_up_params)
      resource_saved = resource.save
      yield resource if block_given?
  
      # What's the result ?
      if !resource_saved
        registration_failed
      elsif resource.active_for_authentication?
        create_and_active
      else
        create_not_active
      end

    rescue StandardError => e
      registration_error

    end

    # Send response
    respond_to do |format|
      format.html {redirect_to root_path(tab: :register) }
      format.js { render("/bmcrails/registration", status: :ok) }
    end
  
  end


private

  def create_and_active
    puts "RegistrationsController#create_and_active"
    sign_up(resource_name, resource)

    flash[:success] = t("devise.registrations.signed_up")
  end

  def create_not_active
    puts "RegistrationsController#create_not_active"
    expire_data_after_sign_in!

    flash[:success] = t("devise.registrations.signed_up_but_#{resource.inactive_message}")
  end

  def registration_failed
    puts "RegistrationsController#create: registration_failed"
    clean_up_passwords(resource)

    flash[:error] = t("devise.bmcrails_signup_failed")
  end

  def registration_error
    puts "RegistrationsController#create: registration_error"
    clean_up_passwords(resource)

    flash[:error] = t("devise.bmcrails_signup_error")
  end

end
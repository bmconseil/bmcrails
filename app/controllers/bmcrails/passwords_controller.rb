class Bmcrails::PasswordsController < Devise::PasswordsController

    def create
      self.resource = resource_class.send_reset_password_instructions(resource_params)
  
      if successfully_sent?(resource)
        flash[:success] = t("devise.bmcrails_password_sent")
      else
        flash[:error] = "error while sending password"
      end


    # Send response
    respond_to do |format|
      format.html {redirect_to root_path(tab: :help) }
      format.js { render("/bmcrails/password", status: :ok) }
    end
  
    end
  
    def new
      super
    end
  
    def update
      super       
    end
  
    def edit
      super   
    end
  end
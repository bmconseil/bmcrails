module Bmcrails
  module ApplicationController
    # protect_from_forgery with: :exception
    extend ActiveSupport::Concern

    # Class methods
    def self.included(base)
      # Include our class methods
      base.extend ApplicationMethods

      # Include our helpers on host controller
      base.include ApplicationHelper
      base.include LocalesHelper
      base.include DeviseHelper
      base.include ToolbarHelper
      base.include NavbarHelper
      base.include PageHelper

      # Common hooks
      base.before_action :set_locale_from_params
      base.before_action :set_locale_from_session
      base.before_action :set_host_and_protocol
      # base.before_action :init_toolbar

      # base.before_action :toolbar_init

      # Authorization
      base.include Pundit
      base.rescue_from Pundit::NotAuthorizedError, with: :error_access_denied
    end

  protected

    # Mailer
    # def set_mailer_default_url_options
    #   ActionMailer::Base.default_url_options[:protocol] = request.protocol
    #   ActionMailer::Base.default_url_options[:host] = request.host_with_port
    # end

    def error_access_denied exception
      policy_name = exception.policy.class.to_s.underscore
      flash[:error] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
  
      # 403 Forbidden response
      respond_to do |format|
        # format.html{ render nothing: true,  :status => 403 }
        format.html{ render '/errors/access_denied',  :status => 403 }
        format.xml{  render :xml => 'Access Denied',  :status => 403 }
        format.json{ render :json => 'Access Denied', :status => 403 }
      end
    end

    def set_host_and_protocol
      # Determine protocol
      proto = ENV['EXTERNAL_PROTOCOL']
      proto ||= request.headers['X-Forwarded-Proto'] if request
      proto ||= request.scheme if request

      # Determine hostname
      host =  ENV['EXTERNAL_HOSTNAME']
      host ||= request.host_with_port if request

      # Configure host for ActionController and ActionMailer
      ActionController::Base.default_url_options[:protocol] = proto
      ActionController::Base.default_url_options[:host]     = host
      ActionMailer::Base.default_url_options[:protocol]     = proto
      ActionMailer::Base.default_url_options[:host]         = host

      # Configure OmniAuth
      OmniAuth.config.full_host = "#{proto}://#{host}"

      debug = {
        OmniAuth: OmniAuth.config.full_host.to_s,
        ActionController: ActionController::Base.default_url_options.to_hash,
        ActionMailer: ActionMailer::Base.default_url_options.to_hash,
      }

      # Rails.logger.debug "set_host_and_protocol #{debug.to_yaml}"
    end
  
  end
end

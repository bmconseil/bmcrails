$(document).ready(function() {

    toastr.options = {
      "closeButton": true,
      "debug": true,
      "positionClass": "toast-top-right",
      "onclick": null,
      "showDuration": "500",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
  
  });
  
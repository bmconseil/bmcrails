$(document).ready(function() {

  // console.log('ajax_init: document ready');

	// Default behaviour form ajax-enabled-forms
	$("form.ajax").submit(function (t){
    // Lock form
    $(this).find('input').prop('disabled', locked);
    $(this).find('.btn-submit').prop('disabled', locked);

    // Send request
		$.ajax({
			url:  $(this).attr('action'),
			type: 'POST',
			data: $(this).serialize(),
			dataType: "script"
			});
		return false;
		});

	// Default behaviour for ajax-enabled-links
	$("a.ajax").click(function() {
		$.ajax({
			url: this.href,
			type: 'GET',
			dataType: "script"
			});
		return false;
		});

  // But we may have classic jQuery events outside of UJS
  $(document).ajaxStart(function(){
    console.log('ajaxStart');
    $(".spinner").show();
  });
  $(document).ajaxStop(function(){
    console.log('ajaxStop');
    $(".spinner").delay(100).hide(0);
  });
  // $(".spinner").hide();

});

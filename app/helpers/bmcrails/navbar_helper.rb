include ActionView::Helpers::TagHelper
include FontAwesome::Rails::IconHelper

NavItem = Struct.new(
  :name, :path, :icon, :if, :text, :title, :group, :style, :method,
  keyword_init: true
)

module Bmcrails
  module NavbarHelper

    # navbar_add > navbar_links_add

    def navbar_links_add name, options = {}
      @navbar_links ||= []
      @navbar_links << NavItem.new(options.merge(name: name))
    end

    def navbar_usermenu_add name, options = {}
      @navbar_usermenu ||= []
      @navbar_usermenu << NavItem.new(options.merge(name: name))
    end

    def navbar_usermenu_divider
      @navbar_usermenu ||= []
      @navbar_usermenu << nil
    end

    def navbar_user user, logout_path
      @navbar_user = user
      @navbar_logout_path = logout_path
    end

    def navbar_logout logout_path
      @navbar_logout_path = logout_path
    end

    def navbar_profile path, user
      @navbar_profile_path = path
      @navbar_user = user
    end

    def navbar_render_links *klasses
      # Ensure init
      @navbar_links = [] unless @navbar_links.is_a? Array

      klass = ["nav-link"]
      klass += klasses if klasses.is_a?(Array)

      # Split items into groups based on "group" value
      capture do
        @navbar_links.group_by { |navitem| navitem.group }.map do |group, group_items|
          group_items.map do |navitem|
           concat navbar_render_item(navitem, klass.join(" "))
          end
        end
      end
    end

    def omniauth_provider_to_social provider
      provider.to_s.split('_').first.to_s
    end

    def navbar_render_usermenu
      content_tag(:li, class: 'nav-item dropdown XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX') do
        concat navbar_render_usermenu_button()
        concat navbar_render_usermenu_links()
      end
    end    


  protected

    def navbar_render_usermenu_button
      return unless @navbar_user

      userlabel = fa_icon(
        omniauth_provider_to_social(@navbar_user.provider),
        text: @navbar_user.name
        )
      
      link_to(userlabel, "#",
        class: "nav-link dropdown-toggle nobr",
        id: "navbarDropdown",
        role: "button",
        "data-toggle": "dropdown",
        "aria-haspopup": true,
        "aria-expanded": false
        )
    end

    def navbar_render_usermenu_links
      return unless @navbar_usermenu.is_a?(Array)
      dropdown = []

      # Add usermenu items
      @navbar_usermenu.each do |navitem|
        # dropdown << content_tag(:div, navitem.inspect, class: "YYYYYYYYYYYY")

        if navitem
          dropdown << navbar_render_item(navitem, "dropdown-item")
        else
          dropdown << content_tag(:div, "", class: 'dropdown-divider')
        end
      end

      # Logout
      if @navbar_logout_path
        logout = NavItem.new(
          name: :logout,
          path: @navbar_logout_path,
          icon: 'sign-out',
          method: :delete
        )
        dropdown << content_tag(:div, "", class: 'dropdown-divider')
        dropdown << navbar_render_item(logout, "dropdown-item")
        end

      # Build menu parts
      content_tag(:span,
        dropdown.join.html_safe,
        class: "dropdown-menu dropdown-menu-right",
        "aria-labelledby": "navbarDropdown"
        )
    end


    def navbar_render_item navitem, link_class
      return if !navitem.if.nil? && navitem.if == false

      # Localize text if not provided
      navitem.text ||= I18n.t("navbar.#{navitem.name}", default: navitem.name.to_s)

      # Special icons
      unless navitem.icon
        navitem.icon = case navitem.name
          when :back
            "backward"
          when :delete
            "trash"
        end
      end

      # Add icon if requested
      if navitem.icon
        label = fa_icon(navitem.icon, text: navitem.text, class: "mr-1")
      else
        label = navitem.text
      end

      # Active item ?
      link_active = (navitem.path == request.path) ? "active" : ""
      link_to(
        label,
        navitem.path,
        class: "#{link_class} #{link_active} nobr",
        method: navitem.method,
        title: navitem.title.to_s,
      )
    end

  end
end

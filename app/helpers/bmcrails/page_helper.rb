include ActionView::Helpers::TagHelper
include FontAwesome::Rails::IconHelper

module Bmcrails
  module PageHelper

    def page_dataset dataset
      @page_dataset = dataset
    end

    def page_context value
      @page_context = value
    end

    def page_title value
      @page_title = value
    end

    def page_subtitle value
      @page_subtitle = value
    end

    def page_add_css file
      @page_css ||= []
      @page_css << file
    end

    def page_add_js file
      @page_js ||= []
      @page_js << file
    end

    def page_add_property name, value
      @page_properties ||= []
      @page_properties << [name, value]
    end

    def page_render_css
      return unless @page_css.is_a?(Array)
      @page_css.collect do |file|
        stylesheet_link_tag file, :media => "all"
      end.join("\n").html_safe
    end

    def page_render_js
      return unless @page_js.is_a?(Array)
      @page_js.collect do |file|
        javascript_include_tag file
      end.join("\n").html_safe
    end

    def page_render_proprties
      return unless @page_properties.is_a?(Array)
      @page_properties.collect do |name, value|
        content_tag(:meta, nil, property: name, content: value)
      end.join("\n").html_safe
    end


    def page_render_title params
      # If we have it, that's the right one
      return @page_title unless @page_title.nil?

      # Determine i18n key
      key = "#{params[:controller]}.#{params[:action]}.title"

      # Build translation if this key exists
      I18n.t(key,
        context: @page_context.to_s,
        default: ""
        )
    end

    def page_render_subtitle params
      @page_subtitle.to_s
    end

    def page_render_title params
      # If we have it, that's the right one
      return @page_title unless @page_title.nil?

      # Determine i18n key
      key = "#{params[:controller]}.#{params[:action]}.title"

      # Build translation if this key exists
      I18n.t(key,
        context: @page_context.to_s,
        default: ""
        )
    end

    def page_title_html app_name
      parts = [app_name, page_render_title(params), page_render_subtitle(params)].reject { |txt| txt.to_s.blank? }
      content_tag(:title, parts.join(' | '))
    end


    def page_object_errors object
      # flash[:error] = object.errors.full_messages.join("<br>").html_safe
      return unless object.errors
      flash[:error] = object.errors.full_messages.join("<br/>").html_safe
      # error_messages_for
    end

  protected

  def page_render_pagination params, klass = 'toolbar-item'
    return unless @page_dataset
    content_tag :div, class: klass do
      paginate(@page_dataset) 
    end
  end


  end
end
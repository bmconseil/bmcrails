module Bmcrails
  module ApplicationHelper
    include ActionView::Helpers::TagHelper
    include FontAwesome::Rails::IconHelper

    def bmcrails_hostname
      Socket.gethostname.split('.').first
    end    

    def bmcrails_flash_btn_class(flash_type)
      case flash_type
      when 'success'
        'btn-success'
      when 'error'
        'btn-danger'
      when 'alert'
        'btn-warning'
      when 'notice'
        'btn-info'
      else
        flash_type.to_s
      end
    end

    def bmcrails_image_placeholder path, width, height, options = {}
      if Rails.env.development? && HIDE_VISUALS==true
        path = sprintf("https://dummyimage.com/%dx%d/dddddd/222222", width, height)
      else
        options[:width] = width if width
        options[:height] = height if height
      end
      image_tag asset_path(path), options
    end

    def bmcrails_flash_for object, title = nil
      return if object.errors.empty?
      out = []

      # Compute title
      if title
        out << title
      else
        out << sprintf(
          "%s: %d %s",
          object.model_name.human,
          object.errors.count,
          t("bmcrails.flashes.error_count").pluralize(object.errors.count)
        )
      end
      out << "<br>"

      # Errors
      object.errors.full_messages.each do |msg|
        out << "- "
        out << msg
        out << "<br>"
      end

      # Store this an a flash
      flash[:error] = out.join('').html_safe
    end


    def context_css
      "context-#{controller_name} context-#{controller_name}-#{action_name}"
    end

    def bmcrails_footer_info
      # ENV['BUILD_DATE'] = '2020-07-14T15:14:14Z'
      {
      ca: "#{controller_name}/#{action_name}",
      sha: ENV['CI_COMMIT_SHORT_SHA'],
      date: localized_from_iso8601(ENV['BUILD_DATE']),
      build: ENV['CI_COMMIT_TAG'] || ENV['CI_COMMIT_REF_NAME'],
      # tag: ENV['CI_COMMIT_TAG'],
      # ref: ENV['CI_COMMIT_REF_NAME'],
      # lang: I18n.locale,
      # host: ENV['HOSTNAME'],
    }
    end
  
    def bmcrails_minibtn type, text
      content_tag(:span, text, class: "btn btn-sm btn-disabled btn-#{type} mb-2")
    end
  
  end
end

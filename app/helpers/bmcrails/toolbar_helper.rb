include ActionView::Helpers::TagHelper
include FontAwesome::Rails::IconHelper

module Bmcrails
  module ToolbarHelper

    def toolbar_add key, options = {}
      @toolbar ||= {}
      @toolbar[key] = options
    end

    def toolbar_search options = {}
      @toolbar_search = options
    end

    def toolbar_create options = {}
      @toolbar_create = options
    end

    def toolbar_submit form, *args
      @submit_form = form
      @submit_args = args
    end

protected

  def toolbar_render_buttons params, klass = 'toolbar-item'
    # Ensure init
    @toolbar = {} unless @toolbar.is_a? Hash

    # Collect itmes
    @toolbar.collect do |name, options|
      content_tag :div, class: klass do
        toolbar_render_item(params, name, options)
      end
    end.join.html_safe
  end  

  def toolbar_render_search params, klass = 'toolbar-item'
    return unless @toolbar_search.is_a?(Hash)
    return "NO_PATH" unless @toolbar_search[:path]

    content_tag :div, class: klass do
      simple_form_for :search, url: @toolbar_search[:path], method: :get, html: { class: 'form-inline' } do |f|
        concat f.input :filter, input_html: {placeholder: t('toolbar.search'), value: search_filter, class: "mr-0"}, required: false, label: false, name: 'blah'
        concat link_to(fa_icon(:close), @toolbar_search[:path], class: "btn clear")
        concat button_tag(fa_icon(:search), class: "btn btn-secondary submit")
      end
    end
  end
    
  def toolbar_render_create params, klass = 'toolbar-item'
    return unless @toolbar_create.is_a?(Hash)
    return "NO_FOR" unless @toolbar_create[:for]
    return "NO_FIELD" unless @toolbar_create[:field]

    content_tag :div, class: klass do
      simple_form_for @toolbar_create[:for], html: { class: 'form-inline' } do |f|
        concat f.input(@toolbar_create[:field], input_html: {placeholder: t('toolbar.create'), class: "mr-0"}, required: false, label: false)
        concat button_tag(fa_icon(:plus), class: "btn btn-secondary submit")
      end
    end

  end

  def toolbar_render_submit params, klass = 'toolbar-item'
    # return @submit_args.inspect
    return unless @submit_form && @submit_args

    # Set options
    # @submit_args[:class] ||= "btn btn-primary"
    # @submit_args[:form] = @submit_form.options.dig(:html, :id)
    # text = @submit_args.delete(:text)

    # Generate button
    content_tag :div, class: klass do
      @submit_form.submit *@submit_args, class: "btn btn-success", form: @submit_form.options.dig(:html, :id)
    end
  end
  

  def toolbar_render_item params, name, options
    # return options.inspect

    # Just a spacer ?
    if options.nil? || options == {}
      return content_tag(:span, '', class: "mr-4") 
    end

    # Tweak options
    if options[:delete]
      options[:method] = :delete 
      options[:data] = { confirm: 'Are you sure?' }
    end

    # Styles
    # options[:type] = "primary" if options[:type].nil?
    type = options.delete(:type)
    type ||= "secondary"
    # options[:class] = 

    # Localize text
    unless options[:text]
      key1 = "#{params[:controller]}.#{params[:action]}.#{name}"
      key2 = "toolbar.#{name}"
      options[:text] = I18n.t(key1, default: I18n.t(key2))
    end
    # text = t(options[:text])

    # Special icons
    unless options[:icon]
      options[:icon] = case name
        when :back
          "backward"
        when :delete
          "trash"
      end
    end    

    # Add icon if requested
    if options[:icon]
      options[:text] = fa_icon(options[:icon], text: options[:text])
    end

    # Build link
    case name
      when :search
        # simple_form_for :search, url: contacts_path, method: :get, html: { class: 'form-inline mr-2' } do |f|
        #   concat f.input(:filter, input_html: {value: search_filter, class: "#{options[:class]} mr-1"}, required: false, label: false)
        #   concat f.submit("Search", class: "btn btn-#{type}")
        # end
        # link_to "SERACH", options[:path], class: options[:class], method: options[:method], data: options[:data]

      else
        link_to options[:text], options[:path], class: "#{options[:class]} btn btn-md btn-#{type} mr-1", method: options[:method], data: options[:data], title: options[:title]
      end
    end


  end
end
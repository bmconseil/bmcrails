module Bmcrails
  module LocalesHelper
  
    # Locales
    def set_locale_from_params
      # Set locale from params
      session[:lang] = params[:lang] if params[:lang]
    end

    def set_locale_from_session
      # Extract from session or headers
      session[:lang] ||= request.env['HTTP_ACCEPT_LANGUAGE'].to_s.scan(/^[a-z]{2}/)[0]

      # Last resort, if it's not in the acceptable set, use default value
      unless I18n.available_locales.map(&:to_s).include?(session[:lang])
        session[:lang] = I18n.default_locale
      end

      # Assign this as the current lng
      I18n.locale = session[:lang]
    end

    def localized_from_iso8601 text
      t = Time.parse(text.to_s).in_time_zone
      #t = Time.parse(text.to_s)
      I18n.l(t, format: :bmcrails)
    rescue StandardError
      nil
    end
  
  end
end

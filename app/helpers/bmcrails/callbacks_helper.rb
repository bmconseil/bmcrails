module Bmcrails
    module CallbacksHelper

      def failure
        redirect_to root_path
      end

      def auth_callback klass, kind = nil
        # Create or fetch user
        @user = from_omniauth(klass, request.env["omniauth.auth"])

        # User is already present
        if @user.persisted?
          puts "OmniauthCallbacksController: returning user ID #{@user.id}"
          sign_in @user, :event => :authentication #this will throw if @user is not activated
          set_flash_message(:notice, :success, :kind => kind) if is_navigational_format?

        # It is a new user
        else
          # Rmember auth data to create a regular account if needed
          session["devise.omniauth.auth"] = request.env["omniauth.auth"]
          puts "OmniauthCallbacksController: new user #{@user.inspect} errors #{@user.errors.inspect}"
          @user.skip_confirmation!
        end
        
      end

    protected

      def from_omniauth klass, auth
        puts "OmniauthCallbacksController: from_omniauth #{auth.inspect}"
        klass.where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
          user.email = auth.info.email
          user.name = auth.info.name
          user.token = auth.credentials.token
          user.expires = auth.credentials.expires
          user.expires_at = auth.credentials.expires_at
          user.refresh_token = auth.credentials.refresh_token

          # Extra attributes
          if user.respond_to?(:image) && auth.info.respond_to?(:image)
            user.image = auth.info.image
          end

          # Set a dummy password
          user.password = SecureRandom.urlsafe_base64

          # No need to confirm
          user.skip_confirmation!
        end
      end

      # def new_with_session(params, session)
      #   super.tap do |user|
      #     if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
      #       user.email = data["email"] if user.email.blank?
      #     end
      #   end
      # end

    end
  end
  
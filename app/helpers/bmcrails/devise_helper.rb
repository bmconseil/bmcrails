module Bmcrails
  module DeviseHelper

    # def omniauth_provider_to_social provider
    #   provider.to_s.split('_').first.to_s
    # end

    # helper_method :resource_name, :resource, :devise_mapping, :resource_class

    # def resource_name
    #   :user
    # end
   
    # def resource_class
    #   User
    # end

    def resource
      @resource ||= resource_class.new
    end

    def devise_mapping
      @devise_mapping ||= Devise.mappings[resource_name]
    end

  protected

    # def password_required?
    #   return false unless confirmed?
    #   return false unless self.provider.blank?
    #   super
    # end

  end
end
